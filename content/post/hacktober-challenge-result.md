--- 
title: "Hacktober challege 2017 comes to an end"
date: 2017-11-10
subtitle: "Open hardware challenge results"
tags: ['activity', 'open-challenge', 'updates', 'hacktober', 'result']
---

![poster](/web.jpg "open token challenge")
On 21th October 2017, FLOSS Cell, Gec Wayand announced an open hardware challenge for building cost effective token display system. Participants were allowed to choose any of the platform on their wish. The offered price money was INR 2,000. Later, on 25th of October 2017, the deadline for the challenge was extended to 30th of October. 

Terms of submission were published on the website.

### Participaltion

All participants were requested to submit their entries via email and also, they were requested to register on a google form. 
Sadly, None of any participation were reached. 

### Complexity

Actual challenge was to generate a unique sound. 
All the rest were available on the web. Arduino Create, a portal for hackers, had published how to design and develop a token display unit. ( https://create.arduino.cc/projecthub/Isaac100/control-a-7-segment-display-with-a-keypad-4ca90a, https://create.arduino.cc/projecthub/driewe/matrix-keypad-with-7-segment-display-31a409 ).
Playing sound were also guided by Arduino Create. Combining both would result the solution. 

### Vote of thanks
We, FLOSS Cell Gec Wayanad for of your kind support and time spend for this challenge. For the teachers, Industry people, Faculties and also YOU!