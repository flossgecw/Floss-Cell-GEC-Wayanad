---
title: "Hacktober challege 2017 deadline extended"
date: 2017-10-25
subtitle: "Open hardware challenge date updates"
tags: ['activity', 'open-challenge', 'updates', 'hacktober']
#bigimg: [{src: "web.jpg", desc: "poster"},]
---
Hackers!

Very happy to announce #Hacktober challenge deadline updates. 
updated deadline for your submission has been updated to 
##### 30-10-2017

### Submission Method

1. Sign up on [link](https://goo.gl/forms/Su9jKbzslfSoq2GK2). 
2. After finishing your project, Send email with all related assets to  `joicey@yandex.com`.

##### Happy Hacking




