---
title: "Open Challenge - Token display unit"
date: 2017-10-21
subtitle: "An open challenge for cheapest token display unit"
tags: ['activity', 'open-challenge', 'hacktober']
#bigimg: [{src: "web.jpg", desc: "poster"},]
---
![poster](/web.jpg "open token challenge")
FLOSS Cell GECW happily announces an open challenge for building a cost effective ,user friendly token display unit.


### Token Display Unit

The solution is everyman’s interface in our daily life where crowd comes to take their turn to get the service.Token no is an everyday reality of people waiting for their turn at banks ,hospital etc.

### Functionals. 
When a trigger/button, is pressed, the 7 segment display will update with a number, and also makes a unique sound which could be tried as a combination of multiple sounds.

For example, train number A reached a station, supposedly station X, the narration will be “Train number A reached station X.” or “station X welcomes passengers of train A”.

In this case, The voice over is unique for train A.


### Terms and conditions.


#### The device should trigger a unique sound

The device should produce a unique sound while the trigger applied. It could be

a combination of multiple soundtracks. The final output should be a unique sound

for the number displayed on the 7 segment display.


#### Matrix keyboard should be used.

Matrix keyboard is used to trigger the display.

The device should listen previous, next, and also numeric numbers ranging from 0 to 9.

When we presses `NEXT` , display should be updated and has to

play soundtrack and vice versa.


For numbers, when numbers are pressed, display should update and also should play

sound.It should be able to jump from one number to an arbitrary number taken from the keyboard.

You can define what should be the delimiter for triggering numeric input.


#### 3 units of seven segment display should used.


The display should show the numbers on trigger and the data should persist there

until next trigger was pressed. After a power failure, last displayed number should

shown on the display.

(the counter should count from 000-999)


#### Single or group entries will be allowed.

single or group entries won't have any priority during the assessment


#### Submission shall be in simulation format.

Bingo. No need of real hardware. The submission shall be in either `qucs` or `proteus` simulation format.

If you are not having that proficiency, then send us working demo video, program you have written for the micro controller, circuit diagram etc.

#### Best and cost effective solution will be selected by the jury from shortlisted entries.

The jury will be of floss cell members, some GECW faculties and industrialists from individual polls.


#### Jury's decision shall be final


#### Its open.

All submitted designs will be in the open domain. The submissions will be uploaded and mirrored on our public version manager repository in gitlab.com.

and also updated on the website as well.


### How to apply

It is quite simple. click on this [link](https://goo.gl/forms/Su9jKbzslfSoq2GK2) and sign up. After finishing your project, just send as an email to `joicey@yandex.com` with your project files. ( we are looking for a better transparent solution. suggest us if you know )


### Deadline

`October 25th 2017` is the final date. You can submit them before `0500` PM Indian Standard Time.

#### Update
Date have been extended to `October 30th 2017`. For more information, read [notification](/post/hacktober-challenge-date-extented/)
### Prize

2K in cash will be awarded for the best and cost effective solution. Winner will be notified award ceremony in person. and also on website.

### Contact

Call `9633598679` anytime IF you are having a doubt except college hours ( 0930-1230,0130-0430 ). In case the call is unanswered, send a message. we will contact you back.

email to `joicey@yandex.com` with title prefix `[open-token-challenge][query]` and we will respond you ASAP.

