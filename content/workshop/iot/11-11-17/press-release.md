--- 
title: "IoT workshop by FLOSS Cell in collaboration with GEC Wayanad Alumni Association"
date: 2017-11-10
subtitle: "Arduino, Iot and fun"
tags: ['activity', 'press-release', 'workshop']
---

![flare](https://00337198940934842109.googlegroups.com/attach/747307e86e25c/IMG-20171108-WA0016.jpg?part=0.1&view=1&vt=ANaJVrFaJV8EO46I99t9kQE18fLhLgie8BDGytJq2np0Ki9-0p1nWxHjhKt2G6H7dousAXBMYklOJh_OwKzW_dlegxTbjXCdQag4uANv6J0xQbYa7I0Im5E, "IoT Workshop flare" )

![flare](https://00337198940934842109.googlegroups.com/attach/747307e86e25c/IMG-20171109-WA0039.jpg?part=0.3&view=1&vt=ANaJVrGF72mu9mM288Kc4BnFqu8kNY9TFIMsu0tZq4Orkqqpp4yyJ5YhH2nM720z-QC7yFVIFY8u02T4z9M-VleaTH8Y28068ptPANDTJtJUtZTaIlFmg5c, "IoT flare")
![flare](https://00337198940934842109.googlegroups.com/attach/747307e86e25c/IMG-20171108-WA0017.jpg?part=0.2&view=1&vt=ANaJVrHwZP7vIsXi9J0ouqcohHkQV9d_JTKsn3f51y8nchQL28j2Hh1PfiNsJMAibL3QzmWig4U2cGk86UwFPwEMOd4vSrlXmvMurEQkaA6SGTriBjYC_Z4, "Iot Flare")
![flare](https://00337198940934842109.googlegroups.com/attach/747307e86e25c/IMG-20171109-WA0037.jpg?part=0.4&view=1&vt=ANaJVrGK9CUeNTyIe13tKMyaTYVpnJ7hs82QVaVtJvrz6Sv7rKG_4e1rPI6gskWJpCfL04OzNx35sWj_JBoBp1ErT6kqBnQ4Ya4FXfnJcR_3zBcwXwnr8RA, "IoT flare")